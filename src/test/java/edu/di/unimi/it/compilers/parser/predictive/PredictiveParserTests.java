package edu.di.unimi.it.compilers.parser.predictive;

import edu.di.unimi.it.compilers.lexer.ExpressionLexer;
import edu.di.unimi.it.compilers.lexer.Lexer;
import edu.di.unimi.it.compilers.parser.ASTNodeMut;
import edu.di.unimi.it.compilers.parser.ParsingErrorException;
import edu.di.unimi.it.compilers.parser.grammar.Grammar;
import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Production;
import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static edu.di.unimi.it.compilers.lexer.ExpressionToken.*;
import static edu.di.unimi.it.compilers.parser.ParserTestingUtils.*;
import static edu.di.unimi.it.compilers.parser.predictive.PredictiveParserTests.ExpressionNonTerminal.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PredictiveParserTests {

    enum ExpressionNonTerminal implements NonTerminal {
        E, F
    }

    private Grammar getGrammar() {
        Production expr = p(E, body(VAR, F));
        Production sum = p(F, body(PLUS, E));
        Production epsilon = p(F, body(Grammar.EPSILON));
        Production parens = p(E, body(OPEN_PAREN, E, CLOSE_PAREN));

        return new Grammar(E, expr, sum, epsilon, parens);
    }

    @Test
    void testSuccessfulParseSimpleExpr() throws Exception {
        ASTNodeMut tree = node(E,
            c(
                node(OPEN_PAREN),
                node(E, c(
                    node(VAR),
                    node(F, c(node(Grammar.EPSILON)))
                )),
                node(CLOSE_PAREN)
            )
        );

        StringReader reader = new StringReader("(a)");
        Lexer lexer = new ExpressionLexer(reader);
        PredictiveParser parser = new PredictiveParser(getGrammar(), lexer);
        assertEquals(tree, parser.parse());
    }

    @Test
    void testSuccessfulParseSum() throws Exception {
        ASTNodeMut tree = node(E,
            c(
                node(OPEN_PAREN),
                node(E, c(
                    node(VAR),
                    node(F, c(
                        node(PLUS),
                        node(E, c(
                            node(VAR),
                            node(F, c(node(Grammar.EPSILON)))
                        ))
                    ))
                )),
                node(CLOSE_PAREN)
            )
        );

        StringReader reader = new StringReader("(a + a)");
        Lexer lexer = new ExpressionLexer(reader);
        PredictiveParser parser = new PredictiveParser(getGrammar(), lexer);
        assertEquals(tree, parser.parse());
    }

    @Test
    void testUnsuccessfulParse() throws Exception {
        StringReader reader = new StringReader("(a + a");
        Lexer lexer = new ExpressionLexer(reader);
        PredictiveParser parser = new PredictiveParser(getGrammar(), lexer);

        assertThrows(ParsingErrorException.class, parser::parse);
    }
}
