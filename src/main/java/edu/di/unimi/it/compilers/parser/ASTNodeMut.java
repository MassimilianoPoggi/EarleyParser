package edu.di.unimi.it.compilers.parser;

import edu.di.unimi.it.compilers.parser.grammar.Symbol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ASTNodeMut {
    private final Symbol symbol;
    private final List<ASTNodeMut> children;

    public ASTNodeMut(final Symbol symbol) {
        this(symbol, new ArrayList<>());
    }

    public ASTNodeMut(final Symbol symbol, final List<ASTNodeMut> children) {
        this.symbol = symbol;
        this.children = children;
    }

    public ASTNodeMut addChild(final ASTNodeMut child) {
        children.add(child);
        return this;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public List<ASTNodeMut> getChildren() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ASTNodeMut astNode = (ASTNodeMut) o;
        return Objects.equals(symbol, astNode.symbol) &&
            Objects.equals(children, astNode.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, children);
    }
}
