package edu.di.unimi.it.compilers.parser;

import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Production;
import edu.di.unimi.it.compilers.parser.grammar.Symbol;

import java.util.Arrays;
import java.util.List;

public class ParserTestingUtils {
    public static Production p(NonTerminal nt, List<Symbol> symbols) {
        return new Production(nt, symbols);
    }

    public static List<Symbol> body(Symbol... symbols) {
        return Arrays.asList(symbols);
    }


    public static ASTNodeMut node(Symbol sym) {
        return new ASTNodeMut(sym);
    }

    public static ASTNodeMut node(Symbol sym, List<ASTNodeMut> children) {
        return new ASTNodeMut(sym, children);
    }

    public static List<ASTNodeMut> c(ASTNodeMut... nodes) {
        return Arrays.asList(nodes);
    }

}
