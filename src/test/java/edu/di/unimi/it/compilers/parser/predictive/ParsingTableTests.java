package edu.di.unimi.it.compilers.parser.predictive;

import edu.di.unimi.it.compilers.parser.grammar.Grammar;
import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Terminal;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static edu.di.unimi.it.compilers.parser.ParserTestingUtils.*;
import static edu.di.unimi.it.compilers.parser.predictive.ParsingTableTests.NonTerminals.*;
import static edu.di.unimi.it.compilers.parser.predictive.ParsingTableTests.Terminals.*;
import static org.junit.jupiter.api.Assertions.*;

class ParsingTableTests {

    public enum Terminals implements Terminal {
        a, b, c, d, EOF
    }

    enum NonTerminals implements NonTerminal {
        S, A, B, C, D
    }


    private Grammar getGrammar() {
        return new Grammar(S,
            p(S, body(A, B)),
            p(S, body(a)),
            p(A, body(C, D)),
            p(B, body(b, A, B)),
            p(B, body(Grammar.EPSILON)),
            p(C, body(d, S, d)),
            p(C, body(c)),
            p(D, body(c, C, D)),
            p(D, body(Grammar.EPSILON))
        );
    }

    @Test
    void testFirst() throws Exception {
        ParsingTable table = new ParsingTable(getGrammar(), EOF);

        Set<Terminal> firstSet = table.first(body(A, B));
        assertEquals(makeSet(c, d), firstSet);

        firstSet = table.first(body(a));
        assertEquals(makeSet(a), firstSet);

        firstSet = table.first(body(C, D));
        assertEquals(makeSet(c, d), firstSet);

        firstSet = table.first(body(b, A, B));
        assertEquals(makeSet(b), firstSet);

        firstSet = table.first(body(d, S, d));
        assertEquals(makeSet(d), firstSet);

        firstSet = table.first(body(c, C, D));
        assertEquals(makeSet(c), firstSet);
    }

    @Test
    void testFollow() throws Exception {
        ParsingTable table = new ParsingTable(getGrammar(), EOF);

        Set<Terminal> followSet = table.follow(B);
        assertEquals(makeSet(EOF, d), followSet);

        followSet = table.follow(D);
        assertEquals(makeSet(EOF, b, d), followSet);

        followSet = table.follow(S);
        assertEquals(makeSet(EOF, d), followSet);
    }

    @Test
    void testFollowRecursion() throws Exception {
        ParsingTable table = new ParsingTable(new Grammar(S,
            p(S, body(a)),
            p(S, body(B, S, A)),
            p(A, body(Grammar.EPSILON)),
            p(B, body(b))
        ), EOF);

        Set<Terminal> followSet = table.follow(A);
        assertEquals(makeSet(EOF), followSet);

        followSet = table.follow(B);
        assertEquals(makeSet(a, b), followSet);

        followSet = table.follow(S);
        assertEquals(makeSet(EOF), followSet);
    }

    private static Set<Terminal> makeSet(Terminal... symbols) {
        return new HashSet<>(Arrays.asList(symbols));
    }
}
