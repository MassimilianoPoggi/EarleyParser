package edu.di.unimi.it.compilers.lexer;

import org.junit.jupiter.api.Test;

import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ExpressionLexerTests {

    @Test
    void testCorrectTokens() throws Exception {
        StringReader reader = new StringReader("a+()*");
        Lexer lexer = new ExpressionLexer(reader);

        assertEquals(ExpressionToken.VAR, lexer.nextToken());
        assertEquals(ExpressionToken.PLUS, lexer.nextToken());
        assertEquals(ExpressionToken.OPEN_PAREN, lexer.nextToken());
        assertEquals(ExpressionToken.CLOSE_PAREN, lexer.nextToken());
        assertEquals(ExpressionToken.BY, lexer.nextToken());
        assertEquals(ExpressionToken.EOF, lexer.nextToken());
    }

    @Test
    void testSkipSpaces() throws Exception {
        StringReader reader = new StringReader("a + a");
        Lexer lexer = new ExpressionLexer(reader);

        assertEquals(ExpressionToken.VAR, lexer.nextToken());
        assertEquals(ExpressionToken.PLUS, lexer.nextToken());
        assertEquals(ExpressionToken.VAR, lexer.nextToken());
        assertEquals(ExpressionToken.EOF, lexer.nextToken());
    }

    @Test
    void testUnrecognizedToken() throws Exception {
        StringReader reader = new StringReader("?");
        Lexer lexer = new ExpressionLexer(reader);

        assertThrows(UnavailableTokenException.class, lexer::nextToken);
    }
}
