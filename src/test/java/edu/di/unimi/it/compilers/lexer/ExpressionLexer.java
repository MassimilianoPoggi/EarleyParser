package edu.di.unimi.it.compilers.lexer;

import edu.di.unimi.it.compilers.parser.grammar.Terminal;

import java.io.IOException;
import java.io.Reader;
import java.util.logging.Logger;

public class ExpressionLexer implements Lexer {
    private static final Logger logger = Logger.getLogger(ExpressionLexer.class.getName());
    private final Reader reader;

    public ExpressionLexer(final Reader reader) {
        this.reader = reader;
    }

    @Override
    public Token nextToken() throws UnavailableTokenException {
        int current = 0;

        try {
            current = reader.read();
        }
        catch (IOException e) {
            logger.severe("Error while reading input: " + e.getMessage());
            throw new RuntimeException();
        }

        if (current == -1) {
            try {
                reader.close();
            }
            catch (IOException e) {
                logger.severe("Error while closing reader: " + e.getMessage());
            }
            finally {
                return ExpressionToken.EOF;
            }
        }

        switch((char)current) {
            case '+':
                return ExpressionToken.PLUS;
            case '*':
                return ExpressionToken.BY;
            case '(':
                return ExpressionToken.OPEN_PAREN;
            case ')':
                return ExpressionToken.CLOSE_PAREN;
            case 'a':
                return ExpressionToken.VAR;
            case ' ':
                return this.nextToken();
            default:
                throw new UnavailableTokenException();
        }
    }

    @Override
    public Terminal getEOFTerminal() {
        return ExpressionToken.EOF.asTerminal();
    }
}