package edu.di.unimi.it.compilers.parser.predictive;

import edu.di.unimi.it.compilers.lexer.Lexer;
import edu.di.unimi.it.compilers.lexer.Token;
import edu.di.unimi.it.compilers.lexer.UnavailableTokenException;
import edu.di.unimi.it.compilers.parser.ASTNodeMut;
import edu.di.unimi.it.compilers.parser.InvalidGrammarTypeException;
import edu.di.unimi.it.compilers.parser.ParsingErrorException;
import edu.di.unimi.it.compilers.parser.grammar.Grammar;
import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Production;
import edu.di.unimi.it.compilers.parser.grammar.Symbol;
import edu.di.unimi.it.compilers.parser.grammar.Terminal;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class PredictiveParser {
    private final Grammar grammar;
    private final Lexer lexer;
    private final ParsingTable table;

    private Token currentToken = null;

    public PredictiveParser(final Grammar grammar, final Lexer lexer) throws InvalidGrammarTypeException {
        this.lexer = lexer;
        this.grammar = grammar;
        this.table = new ParsingTable(grammar, lexer.getEOFTerminal());
    }

    public ASTNodeMut parse() throws ParsingErrorException {
        Deque<ASTNodeMut> stack = new ArrayDeque<>();

        ASTNodeMut root = new ASTNodeMut(grammar.getStartSymbol());
        stack.push(root);
        while (!stack.isEmpty()) {
            ASTNodeMut top = stack.pop();

            Symbol topSym = top.getSymbol();
            if (topSym instanceof Terminal) {
                match((Terminal) topSym);
            } else if (topSym instanceof NonTerminal) {
                Production prod = table.get((NonTerminal) topSym, peek().asTerminal());
                if (prod == null)
                    throw new ParsingErrorException("Could not find production for " + top.getSymbol() + " given token " + peek());

                List<Symbol> body = prod.getRightSide();
                if (body.size() == 1 && body.contains(Grammar.EPSILON)) {
                    top.addChild(new ASTNodeMut(Grammar.EPSILON));
                } else {
                    for (Symbol sym : body)
                        top.addChild(new ASTNodeMut(sym));

                    List<ASTNodeMut> children = top.getChildren();
                    for (int i = children.size() - 1; i >= 0; i--)
                       stack.push(children.get(i));
                }
            }
        }
        return root;
    }

    private Token peek() throws ParsingErrorException {
        if (currentToken == null) {
            try {
                currentToken = lexer.nextToken();
            } catch (UnavailableTokenException e) {
                throw new ParsingErrorException(e);
            }
        }
        return currentToken;
    }

    private void match(Terminal terminal) throws ParsingErrorException {
        Token token = currentToken;
        currentToken = null;
        if (!terminal.equals(token.asTerminal()))
            throw new ParsingErrorException("Could not match terminal " + terminal + " with token " + token);
    }
}
