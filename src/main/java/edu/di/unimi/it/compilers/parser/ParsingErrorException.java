package edu.di.unimi.it.compilers.parser;

public class ParsingErrorException extends Exception {
    public ParsingErrorException() {}

    public ParsingErrorException(String message) {
        super(message);
    }

    public ParsingErrorException(Throwable throwable) {
        super(throwable);
    }
}
