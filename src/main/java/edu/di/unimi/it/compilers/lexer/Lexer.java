package edu.di.unimi.it.compilers.lexer;

import edu.di.unimi.it.compilers.parser.grammar.Terminal;

public interface Lexer {
    Token nextToken() throws UnavailableTokenException;
    Terminal getEOFTerminal();
}
