package edu.di.unimi.it.compilers.parser.earley;

import edu.di.unimi.it.compilers.lexer.ExpressionLexer;
import edu.di.unimi.it.compilers.lexer.ExpressionToken;
import edu.di.unimi.it.compilers.lexer.Lexer;
import edu.di.unimi.it.compilers.parser.ASTNode;
import edu.di.unimi.it.compilers.parser.ParsingErrorException;
import edu.di.unimi.it.compilers.parser.grammar.Grammar;
import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Production;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EarleyParserTests {
    private enum ExpressionNonTerminal implements NonTerminal {
        E;
    }

    private Grammar getExpressionGrammar() {
        Production sum = new Production(ExpressionNonTerminal.E,
                Arrays.asList(ExpressionNonTerminal.E, ExpressionToken.PLUS, ExpressionNonTerminal.E));
        Production mul = new Production(ExpressionNonTerminal.E,
                Arrays.asList(ExpressionNonTerminal.E, ExpressionToken.BY, ExpressionNonTerminal.E));
        Production var = new Production(ExpressionNonTerminal.E,
                Arrays.asList(ExpressionToken.VAR));
        Production parens = new Production(ExpressionNonTerminal.E,
                Arrays.asList(ExpressionToken.OPEN_PAREN, ExpressionNonTerminal.E, ExpressionToken.CLOSE_PAREN));

        return new Grammar(ExpressionNonTerminal.E, sum, mul, var, parens);
    }

    @Test
    void testSuccessfulParse() {
        StringReader reader = new StringReader("(a + a) * a");
        Lexer lexer = new ExpressionLexer(reader);
        EarleyParser parser = new EarleyParser(getExpressionGrammar(), lexer);

        assertTrue(parser.isRecognized());
    }

    @Test
    void testUnsuccessfulParse() {
        StringReader reader = new StringReader("(a + a");
        Lexer lexer = new ExpressionLexer(reader);
        EarleyParser parser = new EarleyParser(getExpressionGrammar(), lexer);

        assertFalse(parser.isRecognized());
    }

    @Test
    void testSuccessfulParseTree() throws ParsingErrorException {
        ASTNode var = new ASTNode(ExpressionToken.VAR, Collections.emptyList());
        ASTNode plus = new ASTNode(ExpressionToken.PLUS, Collections.emptyList());
        ASTNode by = new ASTNode (ExpressionToken.BY, Collections.emptyList());
        ASTNode openParen = new ASTNode(ExpressionToken.OPEN_PAREN, Collections.emptyList());
        ASTNode closeParen = new ASTNode (ExpressionToken.CLOSE_PAREN, Collections.emptyList());

        ASTNode varExpr = new ASTNode(ExpressionNonTerminal.E, Arrays.asList(var));
        ASTNode plusExpr = new ASTNode(ExpressionNonTerminal.E, Arrays.asList(varExpr, plus, varExpr));
        ASTNode parenExpr = new ASTNode(ExpressionNonTerminal.E, Arrays.asList(openParen, plusExpr, closeParen));
        ASTNode multExpr = new ASTNode(ExpressionNonTerminal.E, Arrays.asList(parenExpr, by, varExpr));

        StringReader reader = new StringReader("(a + a) * a");
        Lexer lexer = new ExpressionLexer(reader);
        EarleyParser parser = new EarleyParser(getExpressionGrammar(), lexer);

        assertEquals(multExpr, parser.parse());
    }
}
