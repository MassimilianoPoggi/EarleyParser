package edu.di.unimi.it.compilers.parser;

public class InvalidGrammarTypeException extends Exception {
    public InvalidGrammarTypeException(String message) {
        super(message);
    }
}
