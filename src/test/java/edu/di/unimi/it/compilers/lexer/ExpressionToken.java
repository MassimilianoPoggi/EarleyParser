package edu.di.unimi.it.compilers.lexer;

import edu.di.unimi.it.compilers.parser.grammar.Terminal;

public enum ExpressionToken implements Token {
    OPEN_PAREN("("),
    CLOSE_PAREN(")"),
    PLUS("+"),
    BY("*"),
    VAR("a"),
    EOF("$");

    private final String text;

    ExpressionToken(final String text) {
        this.text = text;
    }

    public Terminal asTerminal() {
        return this;
    }

    @Override
    public String toString() {
        return this.text;
    }
}