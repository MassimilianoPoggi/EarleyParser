package edu.di.unimi.it.compilers.parser.predictive;

import edu.di.unimi.it.compilers.parser.InvalidGrammarTypeException;
import edu.di.unimi.it.compilers.parser.grammar.Grammar;
import edu.di.unimi.it.compilers.parser.grammar.NonTerminal;
import edu.di.unimi.it.compilers.parser.grammar.Production;
import edu.di.unimi.it.compilers.parser.grammar.Symbol;
import edu.di.unimi.it.compilers.parser.grammar.Terminal;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ParsingTable {

    private final Terminal EOF;
    private final Grammar grammar;
    private final Map<NonTerminal, Map<Terminal, Production>> table;

    private final Map<NonTerminal, Set<Terminal>> follows;

    public ParsingTable(final Grammar grammar, final Terminal eof) throws InvalidGrammarTypeException {
        this.grammar = grammar;
        this.EOF = eof;
        this.table = new HashMap<>();
        this.follows = new HashMap<>();
        build();
    }

    private void add(final NonTerminal nt, final Terminal terminal, final Production prod) throws InvalidGrammarTypeException {
        if (table.get(nt).containsKey(terminal)) {
            throw new InvalidGrammarTypeException("The grammar is not LL(1)!");
        }
        table.get(nt).put(terminal, prod);
    }

    private void build() throws InvalidGrammarTypeException {
        buildFollow();
        List<Production> prods = grammar.productions();
        for (Production prod : prods) {
            table.putIfAbsent(prod.getLeftSide(), new HashMap<>());

            Set<Terminal> firstSet = first(prod.getRightSide());
            for (Terminal terminal : firstSet) {
                if (terminal != Grammar.EPSILON) {
                    add(prod.getLeftSide(), terminal, prod);
                }
            }

            if (firstSet.contains(Grammar.EPSILON)) {
                for (Terminal t : follow(prod.getLeftSide()))
                    add(prod.getLeftSide(), t, prod);
            }
        }
    }

    Set<Terminal> first(final List<Symbol> symbols) {
        return first(symbols.get(0));
    }

    private Set<Terminal> first(final Symbol symbol) {
        Set<Terminal> terminals = new LinkedHashSet<>();

        if (symbol instanceof NonTerminal) {
            for (Production p : grammar.productionsFrom((NonTerminal) symbol))
                terminals.addAll(first(p.getRightSide()));
        } else if (symbol instanceof Terminal) {
            terminals.add((Terminal) symbol);
        } else {
            throw new RuntimeException("Unrecognized symbol!");
        }
        return terminals;
    }

    Set<Terminal> follow(final NonTerminal nt) {
        return follows.getOrDefault(nt, Collections.emptySet());
    }

    private void buildFollow() {
        boolean anyUpdate;
        follows.putIfAbsent(grammar.getStartSymbol(), new HashSet<>(Collections.singletonList(EOF)));
        do {
            anyUpdate = false;
            for (Production prod : grammar.productions()) {
                List<Symbol> body = prod.getRightSide();
                for (int i = 0; i < body.size() - 1; i++) {
                    Symbol sym = body.get(i);
                    if (sym instanceof NonTerminal) {
                        NonTerminal nt = (NonTerminal) sym;
                        Set<Terminal> firstSet = first(body.get(i + 1));
                        if (firstSet.contains(Grammar.EPSILON))
                            anyUpdate |= addToFollow(nt, follow(prod.getLeftSide()));
                        firstSet.remove(Grammar.EPSILON);
                        anyUpdate |= addToFollow(nt, firstSet);
                    }
                }

                if (body.get(body.size() - 1) instanceof NonTerminal) {
                    NonTerminal nt = (NonTerminal) body.get(body.size() - 1);
                    anyUpdate |= addToFollow(nt, follow(prod.getLeftSide()));
                }
            }
        } while (anyUpdate);
    }

    private boolean addToFollow(final NonTerminal nt, final Set<Terminal> terminals) {
        if (follows.containsKey(nt)) {
            int oldSize = follows.get(nt).size();
            follows.get(nt).addAll(terminals);
            return follows.get(nt).size() != oldSize;
        } else {
            follows.put(nt, new HashSet<>(terminals));
            return true;
        }
    }

    public Production get(final NonTerminal nt, final Terminal t) {
        return table.get(nt).get(t);
    }
}
