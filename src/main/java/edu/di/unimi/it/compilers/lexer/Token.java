package edu.di.unimi.it.compilers.lexer;

import edu.di.unimi.it.compilers.parser.grammar.Terminal;

public interface Token extends Terminal {
    Terminal asTerminal();
}
