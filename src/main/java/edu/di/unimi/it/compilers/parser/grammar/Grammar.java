package edu.di.unimi.it.compilers.parser.grammar;

import java.util.*;
import java.util.stream.Collectors;

public class Grammar {
    public static final Terminal EPSILON = new Terminal() {
        @Override
        public String toString() {
            return "ε";
        }
    };

    private final Map<NonTerminal, List<Production>> productions;
    private final NonTerminal startSymbol;

    public Grammar(final NonTerminal startSymbol, final Production... productions) {
        this.startSymbol = startSymbol;

        this.productions = new HashMap<>();
        for (Production production : productions) {
            final List<Production> prodList = this.productions.getOrDefault(production.getLeftSide(), new ArrayList<>());
            prodList.add(production);
            this.productions.putIfAbsent(production.getLeftSide(), prodList);
        }
    }

    public List<Production> productionsFrom(final NonTerminal nonTerminal) {
        return Collections.unmodifiableList(this.productions.getOrDefault(nonTerminal, Collections.emptyList()));
    }

    public NonTerminal getStartSymbol() {
        return startSymbol;
    }

    public List<Production> productions() {
        return productions.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
    }
}
